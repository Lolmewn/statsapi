/* 
 * The MIT License
 *
 * Copyright 2015 Sybren Gjaltema.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package nl.lolmewn.stats.stat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import nl.lolmewn.stats.api.stat.StatEntry;

/**
 *
 * @author Lolmewn
 */
public class DefaultStatEntry implements StatEntry {

    private double value;
    private final Map<String, Object> parameters;

    /**
     * Adds an empty entry with an empty parameter set. Please note that {@link #getValue()
     * } will return 0 due to that being the default value for doubles.
     */
    public DefaultStatEntry() {
        this.parameters = new HashMap<>();
    }

    /**
     * Adds an entry with the specified value and an empty parameter set.
     * @param value value for this entry
     */
    public DefaultStatEntry(double value) {
        this();
        this.value = value;
    }

    /**
     * Adds an entry with the specified value and as many parameters as passed to the constructor.
     * @param value value for this entry
     * @param params parameters for this entry. Usually the who/what/when
     */
    public DefaultStatEntry(double value, MetadataPair... params) {
        this(value);
        if (params != null) {
            for (MetadataPair pair : params) {
                parameters.put(pair.getKey(), pair.getValue());
            }
        }
    }

    /**
     * Adds an entry with the specified value and as many parameters as passed to the constructor.
     * @param value value for this entry
     * @param params parameters for this entry. Usually the who/what/when
     */
    public DefaultStatEntry(double value, List<MetadataPair> params) {
        this(value);
        if (params != null) {
            for (MetadataPair pair : params) {
                parameters.put(pair.getKey(), pair.getValue());
            }
        }
    }

    /**
     * Adds an entry with the specified value and as many parameters as passed to the constructor.
     * @param value value for this entry
     * @param params parameters for this entry. Usually the who/what/when
     */
    public DefaultStatEntry(double value, Map<String, Object> params) {
        this.value = value;
        this.parameters = params;
    }

    @Override
    public double getValue() {
        return value;
    }

    @Override
    public Map<String, Object> getMetadata() {
        return this.parameters;
    }

    @Override
    public void setValue(double value) {
        this.value = value;
    }

}
