/* 
 * The MIT License
 *
 * Copyright 2015 Sybren Gjaltema.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package nl.lolmewn.stats.storage;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;
import nl.lolmewn.stats.api.StatManager;
import nl.lolmewn.stats.api.stat.Stat;
import nl.lolmewn.stats.api.stat.StatEntry;
import nl.lolmewn.stats.api.storage.StorageEngine;
import nl.lolmewn.stats.api.storage.StorageException;
import nl.lolmewn.stats.api.user.StatsHolder;
import nl.lolmewn.stats.user.DefaultStatsHolder;

/**
 *
 * @author Lolmewn
 */
public class FlatfileStorageEngine implements StorageEngine {

    private final File rootFolder;
    private final StatManager statManager;
    private Gson gson;
    private boolean enabled = false;

    public FlatfileStorageEngine(StatManager statManager) {
        this(new File("stats" + File.pathSeparator), statManager);
    }

    public FlatfileStorageEngine(File rootFolder, StatManager statManager) {
        this.rootFolder = rootFolder;
        this.statManager = statManager;
    }

    @Override
    public StatsHolder load(UUID userUuid, StatManager statManager) throws StorageException {
        File file = new File(rootFolder, userUuid.toString() + ".dat");
        if (!file.exists()) {
            return new DefaultStatsHolder(userUuid); // No file exists, we return an empty holder
        }
        try {
            return gson.fromJson(new BufferedReader(new FileReader(file)), DefaultStatsHolder.class);
        } catch (FileNotFoundException ex) {
            throw new StorageException("Something went wrong while loading the user!", ex);
        }
    }

    @Override
    public void save(StatsHolder user) throws StorageException {
        File file = new File(rootFolder, user.getUuid().toString() + ".dat");
        if (!file.exists()) {
            try {
                rootFolder.mkdirs();
                file.createNewFile();
            } catch (IOException ex) {
                throw new StorageException("Something went wrong while saving the user!", ex);
            }
        }
        try (FileWriter out = new FileWriter(file, false)) {
            out.write(gson.toJson(user));
        } catch (IOException ex) {
            throw new StorageException("Something went wrong while saving the user!", ex);
        }
    }

    @Override
    public void delete(StatsHolder user) throws StorageException {
        // just delete the file lol
        File file = new File(rootFolder, user.getUuid().toString() + ".dat");
        if (!file.exists()) {
            // ...
            return;
        }
        if (!file.delete()) {
            throw new StorageException("File could not be deleted for " + user.getUuid().toString());
        }
    }

    @Override
    public void enable() throws StorageException {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeHierarchyAdapter(Stat.class, new StatGsonAdapter(statManager));
        builder.registerTypeAdapter(StatEntry.class, new StatEntryGsonCreator());
        builder.enableComplexMapKeySerialization();
        builder.setPrettyPrinting();
        this.gson = builder.create();
        this.enabled = true;
    }

    @Override
    public void disable() throws StorageException {
        this.gson = null;
        this.enabled = false;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

}
