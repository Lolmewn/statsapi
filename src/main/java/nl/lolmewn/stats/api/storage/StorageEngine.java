/* 
 * The MIT License
 *
 * Copyright 2015 Sybren Gjaltema.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package nl.lolmewn.stats.api.storage;

import java.util.UUID;
import nl.lolmewn.stats.api.StatManager;
import nl.lolmewn.stats.api.user.StatsHolder;

/**
 *
 * @author Lolmewn
 */
public interface StorageEngine {

    /**
     * Enables this StorageEngine. Things such as connecting to databases or
     * setting up files should be done here.
     *
     * @throws StorageException Thrown when something went wrong while enabling
     * the engine (SQLException for example)
     */
    public void enable() throws StorageException;

    /**
     * Disables this StorageEngine. Disconnecting and cleaning up resources
     * should be done here.
     *
     * @throws StorageException Thrown when something went wrong while disabling
     * the engine (SQLException for example)
     */
    public void disable() throws StorageException;

    /**
     * Whether or not this instance is enabled. This is usually true after
     * having called {@link #enable() }.
     * @return true if this StorageEngine is enabled, false otherwise
     */
    public boolean isEnabled();

    /**
     * Removes all stats for a given
     * {@link nl.lolmewn.stats.api.user.StatsHolder}. Please note: the actual
     * {@link nl.lolmewn.stats.api.user.StatsHolder} object won't be affected
     *
     * @param user {@link nl.lolmewn.stats.api.user.StatsHolder} to clear all
     * data for
     * @throws StorageException Thrown when something went wrong while removing
     * the user (IOException for example)
     */
    public void delete(StatsHolder user) throws StorageException;

    /**
     * Loads the stats for user by given {@link java.util.UUID}.
     *
     * @param userUuid Unique user ID to load data for
     * @param statManager The manager of Stats, most storage engines will want
     * to iterate over all available stats
     * @return a loaded {@link nl.lolmewn.stats.api.user.StatsHolder} object, or
     * empty if none existed yet
     * @throws StorageException Thrown when something went wrong while loading
     * the user (IOException for example)
     */
    public StatsHolder load(UUID userUuid, StatManager statManager) throws StorageException;

    /**
     * Saves the stats for a given {@link nl.lolmewn.stats.api.user.StatsHolder}
     *
     * @param user {@link nl.lolmewn.stats.api.user.StatsHolder} to save all
     * data for
     * @throws StorageException Thrown when something went wrong while removing
     * the user (IOException for example)
     */
    public void save(StatsHolder user) throws StorageException;

}
