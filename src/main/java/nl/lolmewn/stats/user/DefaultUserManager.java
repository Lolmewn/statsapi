/* 
 * The MIT License
 *
 * Copyright 2015 Sybren Gjaltema.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package nl.lolmewn.stats.user;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.api.StatManager;
import nl.lolmewn.stats.api.storage.StorageEngine;
import nl.lolmewn.stats.api.storage.StorageException;
import nl.lolmewn.stats.api.user.StatsHolder;
import nl.lolmewn.stats.api.user.UserManager;
import nl.lolmewn.stats.storage.FlatfileStorageEngine;

/**
 *
 * @author Lolmewn
 */
public class DefaultUserManager implements UserManager {

    private final Map<UUID, StatsHolder> users;
    private final StorageEngine storageEngine;

    public DefaultUserManager(StatManager statManager) {
        this(new FlatfileStorageEngine(statManager));
    }

    public DefaultUserManager(StorageEngine engine) {
        this.users = new ConcurrentHashMap<>();
        this.storageEngine = engine;
    }

    @Override
    public void addUser(StatsHolder user) {
        this.users.put(user.getUuid(), user);
    }

    @Override
    public StatsHolder getUser(UUID uuid) {
        return this.users.get(uuid);
    }

    @Override
    public Collection<StatsHolder> getUsers() {
        return this.users.values();
    }

    @Override
    public StatsHolder loadUser(UUID uuid, StatManager statManager) throws StorageException {
        return this.storageEngine.load(uuid, statManager);
    }

    @Override
    public void removeUser(UUID uuid) {
        this.users.remove(uuid);
    }

    @Override
    public void saveUser(UUID uuid) throws StorageException {
        this.storageEngine.save(this.getUser(uuid));
    }

    @Override
    public void resetUser(UUID uuid) {
        if(!this.users.containsKey(uuid)){
            return;
        }
        try {
            this.storageEngine.delete(this.getUser(uuid));
        } catch (StorageException ex) {
            Logger.getLogger(DefaultUserManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.users.get(uuid).getStats().clear(); //TODO check if this actually deletes the underlying data and is not just a copy
    }

}
