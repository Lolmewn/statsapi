/* 
 * The MIT License
 *
 * Copyright 2015 Sybren Gjaltema.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package nl.lolmewn.stats.user;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.UUID;
import nl.lolmewn.stats.api.stat.Stat;
import nl.lolmewn.stats.api.stat.StatEntry;
import nl.lolmewn.stats.api.user.StatsHolder;

/**
 *
 * @author Lolmewn
 */
public class DefaultStatsHolder implements StatsHolder {

    private final UUID uuid;
    private final HashMap<Stat, LinkedList<StatEntry>> entries;

    public DefaultStatsHolder(UUID uuid) {
        this.uuid = uuid;
        this.entries = new HashMap<>();
    }

    @Override
    public void addEntry(Stat stat, StatEntry entry) {
        if (!this.hasStat(stat)) {
            this.entries.put(stat, new LinkedList<StatEntry>());
        }
        this.entries.get(stat).add(entry);
    }

    @Override
    public Collection<Stat> getStats() {
        return entries.keySet();
    }

    @Override
    public Collection<StatEntry> getStats(Stat stat) {
        return entries.get(stat);
    }

    @Override
    public UUID getUuid() {
        return uuid;
    }

    @Override
    public boolean hasStat(Stat stat) {
        return entries.containsKey(stat);
    }

    @Override
    public void removeStat(Stat stat) {
        this.entries.remove(stat);
    }

    @Override
    public void removeEntry(Stat stat, StatEntry entry) {
        this.entries.get(stat).remove(entry);
    }

}
