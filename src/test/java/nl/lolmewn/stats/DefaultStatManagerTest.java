package nl.lolmewn.stats;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import nl.lolmewn.stats.api.StatManager;
import nl.lolmewn.stats.api.stat.Stat;
import nl.lolmewn.stats.api.stat.StatEntry;
import nl.lolmewn.stats.api.storage.DataType;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Lolmewn
 */
public class DefaultStatManagerTest {

    private final StatManager instance;
    private Stat stat;

    public DefaultStatManagerTest() {
        this.instance = new DefaultStatManager();
    }

    @Before
    public void setUp() {
        stat = new TestStat();
    }
    
    @Test
    public void testClass(){
        testAddStat();
        testGetStat();
        testGetStats();
    }
    
    public void testAddStat() {
        System.out.println("addStat");
        assertEquals(instance.getStats().size(), 0);
        instance.addStat(stat);
        assertEquals(instance.getStats().size(), 1);
        instance.addStat(stat);
        assertEquals(instance.getStats().size(), 1);
    }
    
    public void testGetStat() {
        System.out.println("getStat");
        String name = "Test Stat";
        Stat expResult = this.stat;
        Stat result = instance.getStat(name);
        assertEquals(expResult, result);
    }
    
    public void testGetStats() {
        System.out.println("getStats");
        Collection<Stat> result = instance.getStats();
        assertEquals(1, result.size());
        for(Stat stat : result){
            assertEquals(stat, this.stat);
        }
    }

    private class TestStat implements Stat {

        private boolean enabled = true;

        @Override
        public String format(StatEntry entry) {
            return getName() + ": " + entry.getValue();
        }

        @Override
        public Map<String, DataType> getDataTypes() {
            return new HashMap<String, DataType>() {
                {
                    put("key", DataType.STRING);
                    put("anotherKey", DataType.STRING);
                }
            };
        }

        @Override
        public String getDescription() {
            return "Test stat";
        }

        @Override
        public String getName() {
            return "Test Stat";
        }

        @Override
        public boolean isEnabled() {
            return enabled;
        }

        @Override
        public void setEnabled(boolean value) {
            enabled = value;
        }

    }

}
